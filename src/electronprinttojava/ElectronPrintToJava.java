package electronprinttojava;

import java.awt.print.Book;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.printing.PDFPageable;
import org.apache.pdfbox.printing.PDFPrintable;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ElectronPrintToJava {

    private String fileName;
    private String printName;
    private int widthPaper;
    private PDDocument buffer;
    PrintService impressora = null;
    private PrintService[] printers = PrinterJob.lookupPrintServices();
    
    ElectronPrintToJava() {
    }
    
    public static void main(String[] args) {
        try {
            ElectronPrintToJava electronPrintToJava = new ElectronPrintToJava();
            electronPrintToJava.iniciarAplicacao(args);
        } catch (IOException | ParseException | PrinterException ex) {
            Logger.getLogger(ElectronPrintToJava.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void iniciarAplicacao(String[] args) throws UnsupportedEncodingException, IOException, FileNotFoundException, ParseException, PrinterException {
        // FAZ A VALIDAÇÃO DOS DADOS DO ARQUIVO
        validPath(args);
        
        // FAZ A LEITURA E CONVERSÃO DO ARQUIVO EM JSON
        tratarArquivo(args[0]);
        
        // INICIA A IMPRESSÃO DO DOCUMENTO
        iniciarImpressao();
    }

    private void validPath (String[] args) throws IllegalArgumentException {
        if (args.length == 0) {
            throw new IllegalArgumentException("Não foi passado o caminho do arquivo de impressão JSON como parametro de entrada do arquivo.");
        } else if (args.length >= 2) {
            throw new IllegalArgumentException("Número de argumentos registrado é inválido.");
        } else if(".json".equals(args[0]) && ".JSON".equals(args[0])) {
            throw new IllegalArgumentException("O caminho(" + args[0] + ") recebido não é um arquivo de extensão .json");
        }        
    }

    private void tratarArquivo(String filePath) throws FileNotFoundException, UnsupportedEncodingException, IOException, ParseException {
        // ABRE O ARQUIVO E PEGA O CONTEUDO
        FileInputStream fileInputStream = new FileInputStream(new File(filePath));
        InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, "UTF-8");
        BufferedReader conteudo = new BufferedReader(inputStreamReader);

        // TRANSFORMA O CONTEUDO DO ARQUIVO EM UM JSON
        JSONObject file = (JSONObject) new JSONParser().parse(conteudo);

        // FECHA O ARQUIVO QUE FOI ABERTO
        fileInputStream.close();
        inputStreamReader.close();
        
        // VERIFICA SE TEM O CONTEUDO A SER IMPRESSO
        if (!file.containsKey("content") || "".equals(file.get("content").toString())) {
            throw new IllegalArgumentException("Conteudo que deve ser impresso não foi informado na chave content");
        }
        
        // SETA O NOME DO ARQUIVO QUE APARECE NA FILA DE IMPRESSÃO
        if (!file.containsKey("fileName") || "".equals(file.get("fileName").toString())) {
            file.put("fileName", "Electron-Print-To-Java-" + new Timestamp(System.currentTimeMillis()).getTime() + ".pdf");
        }
        
        // SETA A IMPRESSORA PADRÃO CASO NÃO SEJA INFORMADO
        if (!file.containsKey("printer") || "".equals(file.get("printer").toString())) {
            file.put("printer", PrintServiceLookup.lookupDefaultPrintService().getName());
        }
        
        // SETA A IMPRESSORA PADRÃO CASO NÃO SEJA INFORMADO
        if (!file.containsKey("widthPaper") || "".equals(file.get("widthPaper").toString())) {
            file.put("widthPaper", 210);
        }
            
        // SETA O NOME DA IMPRESSORA RECEBIDA
        setFileName(file.get("fileName").toString());
        
        // SETA O NOME DA IMPRESSORA RECEBIDA
        setPrintName(file.get("printer").toString());
        
        // SETA O TAMANHO DA LARGURA DO PAPEL
        setWidthPaper(Integer.parseInt(file.get("widthPaper").toString()));
        
        // LOCALIZA A IMPRESSORA SELECIONADA
        for(PrintService printers : getPrinters()) {
            if(printers.getName().equals(getPrintName())) {
                setImpressora(printers);
                break;
            }
        }
        
        // CONVERTE O VALOR RECEBIDO EM UM ARQUIVO PDF
        setBuffer(PDDocument.load(new ByteArrayInputStream(Base64.getDecoder().decode(file.get("content").toString()))));
    }
    
    private void iniciarImpressao() throws PrinterException, IOException {
        // INICIA IMPRESSÃO
        PrinterJob imprimir = PrinterJob.getPrinterJob();

        // SETA CONTEUDO A SER IMPRESSO
        imprimir.setPageable(new PDFPageable(buffer));
        
        // DEFINE O TAMANHO DO PAPEL PARA IMPRESSÃO
        Paper paper = new Paper();
        paper.setSize(getWidthPaper(), imprimir.defaultPage().getHeight());

        // SETA QUE NÃO TERÁ MARGEM
        paper.setImageableArea(0, 0, paper.getWidth(), paper.getHeight());
        
        // DEFINE O FORMATO DA PAGINA CRIADO
        PageFormat pageFormat = new PageFormat();
        pageFormat.setPaper(paper);

        // CRIA AS PAGINAS DE IMPRESSÃO
        Book book = new Book();
        book.append(new PDFPrintable(buffer), pageFormat, buffer.getNumberOfPages());

        // SETA O CONTEUDO DE IMPRESSAO
        imprimir.setPageable(book);
        
        // SETA O CONTEUDO DE IMPRESSAO
        imprimir.setPageable(book);

        // SETA QUAL É A IMPRESSORA PARA IMPRIMIR
        imprimir.setPrintService(getImpressora());

        // SETA O NOME DO ARQUIVO A SER IMPRESSO
        imprimir.setJobName(getFileName());

        // ENIVIA PARA IMPRESSORA
        imprimir.print();
        
        // FECHA O PDF QUE FOI ABERTO
        buffer.close();
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getPrintName() {
        return printName;
    }

    public void setPrintName(String printName) {
        this.printName = printName;
    }

    public PDDocument getBuffer() {
        return buffer;
    }

    public void setBuffer(PDDocument buffer) {
        this.buffer = buffer;
    }

    public PrintService getImpressora() {
        return impressora;
    }

    public void setImpressora(PrintService impressora) {
        this.impressora = impressora;
    }

    public PrintService[] getPrinters() {
        return printers;
    }

    public void setPrinters(PrintService[] printers) {
        this.printers = printers;
    }

    public int getWidthPaper() {
        return widthPaper;
    }

    public void setWidthPaper(int widthPaper) {
        this.widthPaper = widthPaper;
    }
}
